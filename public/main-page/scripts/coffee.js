/**
 * Updates the localstorage amount for amount of lattes made
 * and calls the updateMoneySaved() to update the on screen number
 */
function addLatte() {
    const lattesMade = parseInt(localStorage.getItem("lattesMade")) || 0;
    localStorage.setItem("lattesMade", lattesMade + 1);
    updateMoneySaved();
}

/**
 * Updates the localstorage amount for amount of cappuccinos made
 * and calls the updateMoneySaved() to update the on screen number
 */
function addMocha() {
    const mochasMade = parseInt(localStorage.getItem("mochasMade")) || 0;
    localStorage.setItem("mochasMade", mochasMade + 1);
    updateMoneySaved();
}

/**
 * Updates the localstorage amount for amount of espressos made
 * and calls the updateMoneySaved() to update the on screen number
 */
function addCappuccino() {
    const cappuccinosMade = parseInt(localStorage.getItem("cappuccinosMade")) || 0;
    localStorage.setItem("cappuccinosMade", cappuccinosMade + 1);
    updateMoneySaved();

}

/**
 * Updates the localstorage amount for amount of espressos made
 * and calls the updateMoneySaved() to update the on screen number
 */
function addEspresso() {
    const espressosMade = parseInt(localStorage.getItem("espressosMade")) || 0;
    localStorage.setItem("espressosMade", espressosMade + 1);
    updateMoneySaved();
}

/**
 * Calculates the amount of money saved using starbucks prices
 * and the prices I pay for beans and milk.
 * @returns the amount of money saved by brewing my own coffee
 */
function calculateMoneySaved() {
    // Starbucks costs
    const latteCostStarbucks = 5.25;
    const mochaCostStarbucks = 5.65;
    const cappuccinoCostStarbucks = 5.25;
    const espressoCostStarbucks = 3.25;

    // Personal resources costs 
    const singleEspressoShotCost = (10.09 / 340) * 8;   // (price / grams) * grams
    const cupOfMilkCost = (2.29 * 1.075) / 8;           // (half gallon price / FC tax) / cups in a half gallon

    // Cost to make own drinks
    const myLatteCost = singleEspressoShotCost + cupOfMilkCost;
    const myMochaCost = singleEspressoShotCost + cupOfMilkCost;
    const myCappuccinoCost = singleEspressoShotCost + cupOfMilkCost;
    const myEspressoCost = singleEspressoShotCost;

    const lattesMade = parseInt(localStorage.getItem("lattesMade")) || 0;
    const mochasMade = parseInt(localStorage.getItem("mochasMade")) || 0;
    const cappuccinosMade = parseInt(localStorage.getItem("cappuccinosMade")) || 0;
    const espressosMade = parseInt(localStorage.getItem("espressosMade")) || 0;

    const resourceMoneySaved = ((latteCostStarbucks * lattesMade) - (myLatteCost * lattesMade)) +
        ((mochaCostStarbucks * mochasMade) - (myMochaCost * mochasMade)) +
        ((cappuccinoCostStarbucks * cappuccinosMade) - (myCappuccinoCost * cappuccinosMade)) +
        ((espressoCostStarbucks * espressosMade) - (myEspressoCost * espressosMade));

    // Equipment prices
    const coffeeMachineCost = 361.20;
    const grinderCost = 150.48;
    const frotherCupAndWDT = 13.66;
    const totalEquipmentCost = coffeeMachineCost + grinderCost + frotherCupAndWDT;

    return resourceMoneySaved - totalEquipmentCost;
}

/**
 * Calculates the average coffees made each week since September 10, 2024
 */
function updateAvgWeeklyCoffees() {
    const lattesMade = parseInt(localStorage.getItem("lattesMade")) || 0;
    const mochasMade = parseInt(localStorage.getItem("mochasMade")) || 0;
    const cappuccinosMade = parseInt(localStorage.getItem("cappuccinosMade")) || 0;
    const espressosMade = parseInt(localStorage.getItem("espressosMade")) || 0;

    // Get the total number of coffee drinks made
    const totalCoffeesMade = lattesMade + mochasMade + cappuccinosMade + espressosMade;

    // Calculate the number of weeks since September 10, 2024
    const startDate = new Date("2024-09-10");
    const currentDate = new Date();
    const weeksSinceStart = Math.max((currentDate - startDate) / (1000 * 60 * 60 * 24 * 7), 1);

    // Calculate the average per week
    const avgWeeklyCoffees = totalCoffeesMade / weeksSinceStart;

    // Display the weekly average
    document.getElementById('avgWeeklyCoffees').innerText = `Weekly Coffees: ${avgWeeklyCoffees.toFixed(2)}`;
}

/**
 * Updates on the on screen amount of money saved
 */
function updateMoneySaved() {
    const moneySaved = calculateMoneySaved();
    const moneySavedHeader = document.getElementById("moneySaved");
    moneySavedHeader.textContent = "Money Saved: $" + moneySaved.toFixed(2);
    updateAvgWeeklyCoffees();
}

// Initiate Display
updateMoneySaved();