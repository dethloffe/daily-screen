/**
 * Updates the time box on the page with humidity
 */
function updateDateTime() {
    const dateTimeElement = document.getElementById('date-time');
    const now = new Date();
    const options = { month: 'long', day: 'numeric' };
    const dateString = now.toLocaleDateString('en-US', options);
    let hours = now.getHours();
    const minutes = now.getMinutes();
    const ampm = hours >= 12 ? 'PM' : 'AM';

    // Add ordinal suffix
    const day = now.getDate();
    let suffix = 'th';
    if (day % 10 === 1 && day !== 11) {
        suffix = 'st';
    } else if (day % 10 === 2 && day !== 12) {
        suffix = 'nd';
    } else if (day % 10 === 3 && day !== 13) {
        suffix = 'rd';
    }

    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'

    const minutesStr = minutes < 10 ? '0' + minutes : minutes;
    const timeString = hours + ':' + minutesStr + ' ' + ampm;
    dateTimeElement.innerHTML = dateString.replace(day, day + suffix) + '<br>' + timeString;
}

/**
 * Updates the date box on the page with temperature
 */
function updateWeatherData(temp, humidity, weatherElement) {
    const weatherHTMLElement = document.getElementById('weather');
    const iconElement = document.getElementById('weather-icon');
    const iconUrl = `https://openweathermap.org/img/wn/${weatherElement}@2x.png`;

    iconElement.src = iconUrl;
    iconElement.style.width = "100px";
    iconElement.style.height = "100px";
    weatherHTMLElement.innerHTML = 'Temperature: ' + temp + '<br>' + 'Humidity: ' + humidity;
}

function updateWeather() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(getWeather, logError);
    } else {
        getWeather({ coords: { latitude: 40.5829505, longitude: -105.0704435 } });
        console.log("Geolocation is not supported by this browser.");
    }
}

function getWeather(position) {
    const lat = position.coords.latitude;
    const lon = position.coords.longitude;

    const apiKey = '8dad3db309e50de33c8cdefbe69cec74';
    const apiUrl = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&units=imperial&appid=${apiKey}`;

    fetch(apiUrl)
        .then(response => response.json())
        .then(data => {
            const temperature = data.main.temp;
            const humidity = data.main.humidity;
            updateWeatherData(`${temperature}°F`, `${humidity}%`, data.weather[0].icon);
        })
        .catch(error => console.log('Error fetching weather data:', error));

    // Set image back to bottom
    const photoContainer = document.getElementById("photo-container");
    photoContainer.className = "photo-container-normal";

    // Hide olivia page
    const oliviaPhotoContainer = document.getElementById("olivia-lambeau-photo-container");
    oliviaPhotoContainer.style.display = "none";
}

function logError(error) {
    // Default location to FC
    getWeather({ coords: { latitude: 40.5829505, longitude: -105.0704435 } });

    // Handle error
    switch (error.code) {
        case error.PERMISSION_DENIED:
            console.log("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            console.log("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            console.log("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            console.log("An unknown error occurred.");
            break;
    }
    updateDateTime();
}

setInterval(updateDateTime, 1000);
setInterval(updateWeather, 30000);

// Initialize Display 
updateWeather();
updateDateTime();