let softwareTotal = localStorage.getItem('totalSoftwareMilesRan') ? parseInt(localStorage.getItem('totalSoftwareMilesRan')) : 0;
// let recordHigh = localStorage.getItem('recordHigh') ? parseInt(localStorage.getItem('recordHigh')) : 0;
let dailyTotal = localStorage.getItem('dailyTotal') ? parseInt(localStorage.getItem('dailyTotal')) : 0;
let daysCounted = localStorage.getItem('daysCounted') ? parseInt(localStorage.getItem('daysCounted')) : 1;
let weeklyTotal = localStorage.getItem('weeklyTotal') ? parseInt(localStorage.getItem('weeklyTotal')) : 0;

/**
 * Adds the {number} parameter amount of miles ran to the total count and the daily count.
 * Saves the total count to local storage
 * @param {*} number 
 */
function addMileage(number) {
    softwareTotal += number;
    dailyTotal += number;
    weeklyTotal += number;

    document.getElementById('counter').innerText = dailyTotal;
    document.getElementById('software-total').innerText = 'Software Total: ' + softwareTotal;
    document.getElementById('weekly-total').innerText = 'Weekly Mileage: ' + weeklyTotal;

    localStorage.setItem('totalSoftwareMilesRan', softwareTotal);
    localStorage.setItem('dailyTotal', dailyTotal);
    localStorage.setItem('weeklyTotal', weeklyTotal);

    // if (dailyTotal > recordHigh) {
    //     recordHigh = dailyTotal;
    //     document.getElementById('recordHigh').innerText = 'Record High: ' + recordHigh;
    //     localStorage.setItem('recordHigh', recordHigh);
    // }

    updateDailyAverage();
}

/**
 * Resets the daily number of miles ran and adjusts the number of days counted.
 * Sets the localStorage numbers.
 */
function resetDailyCount() {
    dailyTotal = 0;
    daysCounted += 1;
    document.getElementById('counter').innerText = dailyTotal;
    localStorage.setItem('totalSoftwareMilesRan', softwareTotal);
    localStorage.setItem('dailyTotal', dailyTotal);
    localStorage.setItem('daysCounted', daysCounted);

    updateDailyAverage();
}

/**
 * Resets the number of miles ran for the week to zero every Monday.
 */
function resetWeeklyCount() {
    weeklyTotal = 0;
    localStorage.setItem('weeklyTotal', weeklyTotal);
    document.getElementById('weekly-total').innerText = 'Weekly Mileage: ' + weeklyTotal;
}

/**
 * Calculates and updates the daily average number of miles ran.
 */
function updateDailyAverage() {
    let dailyAverage = softwareTotal ? (softwareTotal / daysCounted).toFixed(2) : 0;
    document.getElementById('daily-average').innerText = 'Daily Average: ' + parseInt(dailyAverage);
}

// Initialize display
document.getElementById('counter').innerText = dailyTotal;
document.getElementById('weekly-total').innerText = 'Weekly Mileage: ' + weeklyTotal;
document.getElementById('software-total').innerText = 'Software Total: ' + softwareTotal;

if (localStorage.getItem('daysCounted') == null) {
    localStorage.setItem('daysCounted', 1);
}

updateDailyAverage();
