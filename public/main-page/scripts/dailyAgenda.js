const names = ["Mom", "Dad", "Grandma D", "Grandma C", "Sam", "Alex", "Brady", "Will", "Jake", "Adam", "Tyler", "Scout", "Calder", "Brett", "Colin"];
const callListDiv = document.getElementById("callList");

names.forEach(name => {
    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.id = name;
    checkbox.name = name;
    checkbox.style.transform = "scale(1.5)";
    checkbox.style.margin = "10px";

    const label = document.createElement("label");
    label.htmlFor = name;
    label.textContent = name;
    label.style.fontSize = "25px";

    const br = document.createElement("br");

    callListDiv.appendChild(checkbox);
    callListDiv.appendChild(label);
    callListDiv.appendChild(br);
});

document.addEventListener('DOMContentLoaded', (event) => {
    updateMissedCalls();
});

/**
 * Sets the day of the week box and the meal deals based on the day of the week. 
 * Handles unchecking of boxes for phone calls when call day has passed.
 */
function setData() {
    const day = new Date().getDay();
    const daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    const mealDeals = [
        "",
        "Krazy Karls! BEAT THE CLOCK! Between 5-8pm, the time you call is the price you pay for a large cheese pizza!",
        "Food Trucks at City Park!<br>C.B. and Potts! Potts Mac & Cheese - $8<br>C.B. and Potts! Long Island Iced Teas - $5.75",
        "Food Trucks at CSU Lagoon!<br>C.B. and Potts! Any burger and fries - $12",
        "Krazy Karls! BEAT THE CLOCK! Between 5-8pm, the time you call is the price you pay for a large cheese pizza!<br>C.B. and Potts! Steak and Fries - $16",
        "C.B. and Potts! All American Bomb Pop, Keep The Glass - $5.75",
        ""
    ]

    // Set the day of the week
    document.getElementById("dayOfWeek").innerHTML = daysOfWeek[day];

    // Set the meal deals for the day
    if (mealDeals[day] !== "") {
        document.getElementById("mealDealsSection").style.display = "block";
        document.getElementById("mealDeals").innerHTML = mealDeals[day];
    } else {
        document.getElementById("mealDealsSection").style.display = "none";
    }
}

/**
 * Removes the check mark from the box under all names in the call list
 * &
 * adds / removes a reminder if the person had not been called in the last weeks
 */
function uncheckNames() {
    const checkboxes = document.querySelectorAll('#callList input[type="checkbox"]');
    checkboxes.forEach((checkbox) => {
        if (!checkbox.checked) {
            // Call was not made, add a week to last called counter
            let missedCalls = parseInt(localStorage.getItem(checkbox.name + "_missed") || 0);
            missedCalls += 1;
            localStorage.setItem(checkbox.name + "_missed", missedCalls);
        } else {
            // Reset missed calls counter since the person was called
            localStorage.setItem(checkbox.name + "_missed", 0);
        }

        // Uncheck the checkbox
        checkbox.checked = false;
        localStorage.setItem(checkbox.name, false);
    });
}

function updateMissedCalls() {
    const checkboxes = document.querySelectorAll('#callList input[type="checkbox"]');

    // Load saved states
    checkboxes.forEach((checkbox) => {
        const savedState = localStorage.getItem(checkbox.name);
        const label = document.querySelector(`label[for="${checkbox.name}"]`);

        // Check if checkbox input was checked
        if (savedState !== null) {
            checkbox.checked = savedState === 'true';
        }

        // Add event listener to save state on change
        checkbox.addEventListener('change', (e) => {
            localStorage.setItem(checkbox.name, e.target.checked);
            if (e.target.checked) {
                localStorage.setItem(checkbox.name + "_missed", "");
                label.textContent = checkbox.name;
            }
        });

        // Update the label if it has been weeks since a call
        let missedCalls = parseInt(localStorage.getItem(checkbox.name + "_missed") || 0);
        if (missedCalls !== 0) {
            label.textContent = "(" + missedCalls + " wk) " + checkbox.name;
        } else {
            // Reset missed calls counter since the person was called
            localStorage.setItem(checkbox.name + "_missed", "");
            label.textContent = checkbox.name;
        }
    });
}

setData();
setInterval(setData, 1000);