const recordsOwned = [
    { title: "Arrival", band: "Abba", releaseYear: 1976 },
    { title: "Voulez-Vous", band: "Abba", releaseYear: 1979 },
    { title: "Back in Black", band: "AC/DC", releaseYear: 1980 },
    { title: "Dirty Deeds Done Dirt Cheap", band: "AC/DC", releaseYear: 1976 },
    { title: "The Road Starts Hear", band: "Aerosmith", releaseYear: 2021 },
    { title: "Toys In The Attic", band: "Aerosmith", releaseYear: 1975 },
    { title: "Hey Jude", band: "The Beatles", releaseYear: 1970 },
    { title: "Introducing... The Beatles", band: "The Beatles", releaseYear: 1964 },
    { title: "Let It Be", band: "The Beatles", releaseYear: 1970 },
    { title: "Magical Mystery Tour", band: "The Beatles", releaseYear: 1967 },
    { title: "Meet The Beatles", band: "The Beatles", releaseYear: 1964 },
    { title: "Rubber Soul", band: "The Beatles", releaseYear: 1965 },
    { title: "Revolver", band: "The Beatles", releaseYear: 1966 },
    { title: "Sgt. Peppers Lonely Hearts Club Band", band: "The Beatles", releaseYear: 1967 },
    { title: "The White Album", band: "The Beatles", releaseYear: 1968 },
    { title: "With The Beatles", band: "The Beatles", releaseYear: 1963 },
    { title: "Yesterday and Today", band: "The Beatles", releaseYear: 1966 },
    { title: "Waltz For Debby", band: "Bill Evans Trio", releaseYear: 1961 },
    { title: "Don't Say No", band: "Billy Squier", releaseYear: 1981 },
    { title: "Greatest Hits", band: "Bob Dylan", releaseYear: 1967 },
    { title: "Nashville Skyline", band: "Bob Dylan", releaseYear: 1969 },
    { title: "Changes One Bowie", band: "David Bowie", releaseYear: 1976 },
    { title: "The Cars", band: "The Cars", releaseYear: 1978 },
    { title: "Hits Back", band: "The Clash", releaseYear: 2013 },
    { title: "London Calling", band: "The Clash", releaseYear: 1979 },
    { title: "Pyschotic Reaction", band: "Count Five", releaseYear: 1966 },
    { title: "Bayou Country", band: "Creedence Clearwater Revival", releaseYear: 1969 },
    { title: "Cosmo`s Factory", band: "Creedence Clearwater Revival", releaseYear: 1970 },
    { title: "Deja Vu", band: "Crosby, Stills, Nash, & Young", releaseYear: 1970 },
    { title: "Random Access Memories", band: "Daft Punk", releaseYear: 2013 },
    { title: "2001", band: "Dr. Dre", releaseYear: 1999 },
    { title: "L.A. Woman Sessions part 1", band: "The Doors", releaseYear: 1971 },
    { title: "L.A. Woman Sessions part 2", band: "The Doors", releaseYear: 1971 },
    { title: "L.A. Woman Sessions part 3", band: "The Doors", releaseYear: 1971 },
    { title: "L.A. Woman Sessions part 4", band: "The Doors", releaseYear: 1971 },
    { title: "L.A. Woman", band: "The Doors", releaseYear: 1971 },
    { title: "Hotel California", band: "Eagles", releaseYear: 1976 },
    { title: "Desparado", band: "Eagles", releaseYear: 1973 },
    { title: "Eagles", band: "Eagles", releaseYear: 1972 },
    { title: "The Long Run", band: "Eagles", releaseYear: 1979 },
    { title: "One Of These Nights", band: "Eagles", releaseYear: 1975 },
    { title: "Rumours", band: "Fleetwood Mac", releaseYear: 1977 },
    { title: "Frampton Comes Alive", band: "Peter Frampton", releaseYear: 1976 },
    { title: "American Idiot", band: "Green Day", releaseYear: 2004 },
    { title: "Dookie", band: "Green Day", releaseYear: 1994 },
    { title: "The Best Of Guess Who", band: "Guess Who", releaseYear: 1971 },
    { title: "The Best Of Guess Who Volume II", band: "Guess Who", releaseYear: 1973 },
    { title: "Appetite For Destruction", band: "Guns N Roses", releaseYear: 1987 },
    { title: "Lies", band: "Guns N Roses", releaseYear: 1988 },
    { title: "Use Your Illusion I", band: "Guns N Roses", releaseYear: 1991 },
    { title: "Use Your Illusion II", band: "Guns N Roses", releaseYear: 1991 },
    { title: "Dreamboat Annie", band: "Heart", releaseYear: 1975 },
    { title: "Double Fantasy", band: "John Lennon/Yoko Ono", releaseYear: 1980 },
    { title: "John Lennon/Plastic Ono Band", band: "John Lennon", releaseYear: 1970 },
    { title: "Imagine", band: "John Lennon", releaseYear: 1971 },
    { title: "Mind Games", band: "John Lennon", releaseYear: 1973 },
    { title: "Rock N Roll", band: "John Lennon", releaseYear: 1975 },
    { title: "Live at The House of Blues", band: "The Knack", releaseYear: 1998 },
    { title: "Mac Demarco 2", band: "Mac Demarco", releaseYear: 2012 },
    { title: "The $5.98 EP", band: "Metallica", releaseYear: 1987 },
    { title: "Primitive Cool", band: "Mick Jagger", releaseYear: 1987 },
    { title: "Dr. Feelgood", band: "Motley Crue", releaseYear: 1989 },
    { title: "Girls, Girls, Girls", band: "Motley Crue", releaseYear: 1987 },
    { title: "Shout at the Devil", band: "Motley Crue", releaseYear: 1983 },
    { title: "Too Fast For Love", band: "Motley Crue", releaseYear: 1981 },
    { title: "Theatre of Pain", band: "Motley Crue", releaseYear: 1985 },
    { title: "Nevermind", band: "Nirvana", releaseYear: 1991 },
    { title: "Tug of War", band: "Paul McCartney", releaseYear: 1982 },
    { title: "A Momentary Lapse of Reason", band: "Pink Floyd", releaseYear: 1987 },
    { title: "Animals", band: "Pink Floyd", releaseYear: 1977 },
    { title: "A Collection of Great Dance Songs", band: "Pink Floyd", releaseYear: 1981 },
    { title: "Atom Heart Mother", band: "Pink Floyd", releaseYear: 1970 },
    { title: "Meddle", band: "Pink Floyd", releaseYear: 1971 },
    { title: "More", band: "Pink Floyd", releaseYear: 1969 },
    { title: "Obscured By Clouds", band: "Pink Floyd", releaseYear: 1972 },
    { title: "The Final Cut", band: "Pink Floyd", releaseYear: 1983 },
    { title: "A Piper at the Gates of Dawn", band: "Pink Floyd", releaseYear: 1967 },
    { title: "The Wall", band: "Pink Floyd", releaseYear: 1979 },
    { title: "Wish You Were Here", band: "Pink Floyd", releaseYear: 1975 },
    { title: "A Day at the Races", band: "Queen", releaseYear: 1976 },
    { title: "A Night at the Opera", band: "Queen", releaseYear: 1975 },
    { title: "News of the World", band: "Queen", releaseYear: 1977 },
    { "title": "Jazz", "band": "Queen", "releaseYear": 1978 },
    { "title": "Metal Health", "band": "Quiet Riot", "releaseYear": 1983 },
    { "title": "Richard Wright", "band": "Wet Dream", "releaseYear": 1978 },
    { "title": "Hi Infidelity", "band": "REO Speedwagon", "releaseYear": 1981 },
    { "title": "Old Wave", "band": "Ringo Starr", "releaseYear": 1983 },
    { "title": "Goodnight Vienna", "band": "Ringo Starr", "releaseYear": 1974 },
    { "title": "The Pros and Cons of Hitch Hiking", "band": "Roger Waters", "releaseYear": 1984 },
    { "title": "Us + Them", "band": "Roger Waters", "releaseYear": 2018 },
    { "title": "Let It Bleed", "band": "Rolling Stones", "releaseYear": 1969 },
    { "title": "It's Only Rock & Roll", "band": "Rolling Stones", "releaseYear": 1974 },
    { "title": "On Air", "band": "Rolling Stones", "releaseYear": 1998 },
    { "title": "Sticky Fingers", "band": "Rolling Stones", "releaseYear": 1971 },
    { "title": "Some Girls", "band": "Rolling Stones", "releaseYear": 1978 },
    { "title": "Sucking In The Seventies", "band": "Rolling Stones", "releaseYear": 1981 },
    { "title": "Abraxas", "band": "Santana", "releaseYear": 1970 },
    { "title": "Nevermind The Bollocks, Here's The Sex Pistols", "band": "The Sex Pistols", "releaseYear": 1977 },
    { "title": "Doggy Style", "band": "Snoop Dogg", "releaseYear": 1993 },
    { "title": "Can't Buy a Thrill", "band": "Steely Dan", "releaseYear": 1972 },
    { "title": "Gaucho", "band": "Steely Dan", "releaseYear": 1980 },
    { "title": "But Seriously Folks...", "band": "Joe Walsh", "releaseYear": 1978 },
    { "title": "1984", "band": "Van Halen", "releaseYear": 1984 },
    { "title": "Tres Hombres", "band": "ZZ Top", "releaseYear": 1973 },
    { "title": "Amused To Death", "band": "Roger Waters", "releaseYear": 1992 },
    { "title": "Santana", "band": "Santana", "releaseYear": 1969 },
    { "title": "Bitches Brew", "band": "Miles Davis", "releaseYear": 1970 },
    { "title": "Thriller", "band": "Michael Jackson", "releaseYear": 1982 },
    { "title": "Eye In The Sky", "band": "The Alan Parsons Project", "releaseYear": 1982 },
    { "title": "Communique", "band": "Dire Straits", "releaseYear": 1979 },
    { "title": "Dire Straits", "band": "Dire Straits", "releaseYear": 1978 },
    { "title": "Love Over Gold", "band": "Dire Straits", "releaseYear": 1982 },
    { "title": "Electric Warrior", "band": "T. Rex", "releaseYear": 1971 },
    { "title": "Groovin'", "band": "The Young Rascals", "releaseYear": 1967 },
    { "title": "Baby I'm-A Want You", "band": "Bread", "releaseYear": 1972 },
    { "title": "Still Bill", "band": "Bill Withers", "releaseYear": 1972 },
    { "title": "Dark Side of the Moon", "band": "Pink Floyd", "releaseYear": 1973 },
    { "title": "Abbey Road", "band": "The Beatles", "releaseYear": 1969 },
    { "title": "Get The Knack", "band": "The Knack", "releaseYear": 1979 },
    { "title": "Santana III", "band": "Santana", "releaseYear": 1971 },
    { "title": "Aja", "band": "Steely Dan", "releaseYear": 1977 },
    { "title": "Katy Lied", "band": "Steely Dan", "releaseYear": 1975 },
    { "title": "Breakfast In America", "band": "Supertramp", "releaseYear": 1979 }
];

function getRandomIndex(array) {
    return Math.floor(Math.random() * array.length);
}

/**
 * Selects a random album from the recordsOwned array making sure that 
 * a record is not choosen twice before any other is chosen once.
 * 
 * @returns random Album object
 */
function getRandomRecord(records) {
    const chosenIndexes = JSON.parse(localStorage.getItem("chosenIndexes")) || [];

    if (chosenIndexes.length === records.length) {
        // All records have been chosen, reset count
        chosenIndexes.length = 0;
    }

    let randomIndex;

    // Loop until an unchosen index is found
    do {
        randomIndex = getRandomIndex(records);
    } while (chosenIndexes.includes(randomIndex));

    // Add chosen index to the list
    chosenIndexes.push(randomIndex);
    localStorage.setItem("chosenIndexes", JSON.stringify(chosenIndexes));

    return records[randomIndex];
}

/**
 * Displays the randomly chosen record.
 */
function updateRandomRecord() {
    const randomRecord = getRandomRecord(recordsOwned);
    document.getElementById("title").textContent = `Title: ${randomRecord.title}`;
    document.getElementById("band").textContent = `Band: ${randomRecord.band}`;
    localStorage.setItem("title", randomRecord.title);
    localStorage.setItem("band", randomRecord.band);

    // Add album progress
    const numChosen = JSON.parse(localStorage.getItem("chosenIndexes")).length || 0;
    const numTotal = recordsOwned.length;
    document.getElementById("album-rotation-progress").textContent = "Album: " + numChosen + " of " + numTotal;
}

// Initialize Display
let title = localStorage.getItem("title");
let band = localStorage.getItem("band");
if (title !== null && band !== null) {
    document.getElementById("title").textContent = `Title: ${title}`;
    document.getElementById("band").textContent = `Band: ${band}`;

    // Add album progress
    const numChosen = JSON.parse(localStorage.getItem("chosenIndexes")).length || 0;
    const numTotal = recordsOwned.length;
    document.getElementById("album-rotation-progress").textContent = "Album: " + numChosen + " of " + numTotal;
} else {
    updateRandomRecord();
}