function setRandomPhoto() {
    // TODO adjust the git repository to find all 30 images
    // const numberOfPhotos = 30;
    // const randomNumber = Math.floor(Math.random() * numberOfPhotos) + 1;
    // const randomPhoto = `photos/images/photo${randomNumber}.jpg`;

    // document.getElementById('photo-container').src = randomPhoto;
    // localStorage.setItem("photo", randomPhoto);
}

// Initialize Display
// let currentPhoto = localStorage.getItem("photo");
// if (currentPhoto !== null) {
//     document.getElementById('photo-container').src = "../images/" + currentPhoto;
// } else {
//     setRandomPhoto();
// }

const italyImage = document.getElementById("photo-container");
italyImage.addEventListener("click", () => {
    const currentURL = window.location.hostname;

    if (currentURL === "localhost") {
        window.location.href = "http://localhost:3000/images/index.html";
    } else {
        window.location.href = "/photos/";
    }
});

const leambeauImage = document.getElementById("olivia-lambeau-photo-container");
leambeauImage.addEventListener("click", () => {
    const currentURL = window.location.hostname;

    if (currentURL === "localhost") {
        window.location.href = "http://localhost:3000/olivia/index.html";
    } else {
        window.location.href = "/olivias-page/";
    }
});