/**
 * Compares the current time stamp with the previous time stamp to determine
 * if a new day has started.
 * 
 * @returns True if past midnight, False if not
 */
function checkMidnight() {
    const now = new Date();
    const currentTime = now.getTime();
    const lastCheckedTime = localStorage.getItem('lastCheckedTime');
    localStorage.setItem('lastCheckedTime', currentTime);

    if (!lastCheckedTime) {
        return false;
    }

    const lastCheckedDate = new Date(parseInt(lastCheckedTime));
    const lastCheckedDateString = lastCheckedDate.toDateString();
    const currentDateString = now.toDateString();

    return lastCheckedDateString !== currentDateString;
}

/**
 * Calls checkMidnight to see if it is past midnight and handles cases as needed
 */
function checkForDayChange() {
    const day = new Date().getDay();
    const monday = 1;

    if (checkMidnight()) {
        resetDailyCount();
        updateDailyAverage();
        updateRandomRecord();
        setRandomPhoto();
        updateMoneySaved();
        if (day === monday) {
            // Monday
            uncheckNames();
            updateMissedCalls();
            resetWeeklyCount();
        }
    }
}

// Check every minute if it's midnight
setInterval(checkForDayChange, 1000);