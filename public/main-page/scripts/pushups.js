let softwareTotal = localStorage.getItem('pushupsCounted') ? parseInt(localStorage.getItem('pushupsCounted')) : 0;
let recordHigh = localStorage.getItem('recordHigh') ? parseInt(localStorage.getItem('recordHigh')) : 0;
let dailyTotal = localStorage.getItem('dailyTotal') ? parseInt(localStorage.getItem('dailyTotal')) : 0;
let daysCounted = localStorage.getItem('daysCounted') ? parseInt(localStorage.getItem('daysCounted')) : 1;

/**
 * Adds the number parameter amount of pushups to the total count and the daily count.
 * Saves the total count to local storage
 * @param {*} number 
 */
function addPushups(number) {
    softwareTotal += number;
    dailyTotal += number;
    document.getElementById('counter').innerText = dailyTotal;
    document.getElementById('software-total').innerText = 'Software Total: ' + softwareTotal;
    localStorage.setItem('pushupsCounted', softwareTotal);
    localStorage.setItem('dailyTotal', dailyTotal);

    if (dailyTotal > recordHigh) {
        recordHigh = dailyTotal;
        document.getElementById('record-high').innerText = 'Record High: ' + recordHigh;
        localStorage.setItem('recordHigh', recordHigh);
    }

    updateDailyAverage();
}

/**
 * Resets the daily pushups counted and adjusts the number of days counted.
 * Sets the localStorage numbers.
 */
function resetDailyCount() {
    dailyTotal = 0;
    daysCounted += 1;
    document.getElementById('counter').innerText = dailyTotal;
    localStorage.setItem('pushupsCounted', softwareTotal);
    localStorage.setItem('dailyTotal', dailyTotal);
    localStorage.setItem('daysCounted', daysCounted);

    updateDailyAverage();
}

/**
 * Calculates and updates the daily average number of push ups.
 */
function updateDailyAverage() {
    let dailyAverage = softwareTotal ? (softwareTotal / daysCounted).toFixed(2) : 0;
    document.getElementById('daily-average').innerText = 'Daily Average: ' + parseInt(dailyAverage);
}

// Initialize display
document.getElementById('counter').innerText = dailyTotal;
document.getElementById('record-high').innerText = 'Record High: ' + recordHigh;
document.getElementById('software-total').innerText = 'Software Total: ' + softwareTotal;

if (localStorage.getItem('daysCounted') == null) {
    localStorage.setItem('daysCounted', 1);
}

updateDailyAverage();
