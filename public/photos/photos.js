document.addEventListener("DOMContentLoaded", () => {
    const imagesContainer = document.getElementById("imagesContainer");
    const numberOfImages = 30;

    for (let i = 1; i <= numberOfImages; i++) {
        const imgElement = document.createElement("img");
        imgElement.src = `images/photo${i}.jpg`;
        imgElement.alt = `Photo ${i}`;

        imagesContainer.appendChild(imgElement);
        imagesContainer.appendChild(document.createElement("br"));
    }

    const backButton = document.getElementById("backButton");
    backButton.addEventListener("click", () => {
        const currentURL = window.location.hostname;

        if (currentURL === "localhost") {
            window.location.href = "http://localhost:3000/todolist/index.html";
        } else {
            window.location.href = "/main-page/";
        }
    });
});