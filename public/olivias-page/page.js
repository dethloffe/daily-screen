const names = ["Mom (Deanna)", "Dad (Mark)", "Brooke", "Mammy & Bapa", "Gram", "Sophia", "Bri", "Kylee"];
const callListDiv = document.getElementById("callList");

names.forEach(name => {
    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.id = name;
    checkbox.name = name;
    checkbox.style.transform = "scale(1.5)";
    checkbox.style.margin = "10px";

    const label = document.createElement("label");
    label.htmlFor = name;
    label.textContent = name;
    label.style.fontSize = "25px";

    const br = document.createElement("br");

    callListDiv.appendChild(checkbox);
    callListDiv.appendChild(label);
    callListDiv.appendChild(br);
});

document.addEventListener('DOMContentLoaded', (event) => {
    updateMissedCalls();

    const backButton = document.getElementById("backButton");
    backButton.addEventListener("click", () => {
        const currentURL = window.location.hostname;

        if (currentURL === "localhost") {
            window.location.href = "http://localhost:3000/todolist/index.html";
        } else {
            window.location.href = "/main-page/";
        }
    });
});

function updateMissedCalls() {
    const checkboxes = document.querySelectorAll('#callList input[type="checkbox"]');

    // Load saved states
    checkboxes.forEach((checkbox) => {
        const savedState = localStorage.getItem(checkbox.name);
        const label = document.querySelector(`label[for="${checkbox.name}"]`);

        // Check if checkbox input was checked
        if (savedState !== null) {
            checkbox.checked = savedState === 'true';
        }

        // Add event listener to save state on change
        checkbox.addEventListener('change', (e) => {
            localStorage.setItem(checkbox.name, e.target.checked);
            if (e.target.checked) {
                localStorage.setItem(checkbox.name + "_missed", "");
                label.textContent = checkbox.name;
            }
        });

        // Update the label if it has been weeks since a call
        let missedCalls = parseInt(localStorage.getItem(checkbox.name + "_missed") || 0);
        if (missedCalls !== 0) {
            label.textContent = "(" + missedCalls + " wk) " + checkbox.name;
        } else {
            // Reset missed calls counter since the person was called
            localStorage.setItem(checkbox.name + "_missed", "");
            label.textContent = checkbox.name;
        }
    });
}

function setData() {
    const day = new Date().getDay();
    const daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    // Set the day of the week
    document.getElementById("dayOfWeek").innerHTML = daysOfWeek[day];
}

setData();
setInterval(setData, 60000);