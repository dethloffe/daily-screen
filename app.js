const express = require('express');

const app = express();

app.use('/todolist', express.static('./public/main-page'));
app.use('/images', express.static('./public/photos'));
app.use('/olivia', express.static('./public/olivias-page'));

app.use((req, res) => {
    res.status(404);
    res.send('<h1>Error 404: Resource not found</h1>')
})

app.listen(3000, () => {
    console.log("App listening on port 3000");
})